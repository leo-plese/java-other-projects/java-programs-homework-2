package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * 
 * Class <code>ObjectStack</code> representing model of a stack working with objects allowing typical object stack operations such as push, pop and peek.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public class ObjectStack {
	
	/**
	 * Class uses the collection type <code>ArrayIndexedCollection</code> to store its elements.
	 */
	private ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
	
	/**
	 * Method saying if the stack is empty (has no elements).
	 * 
	 * @return true if stack is empty
	 */
	public boolean isEmpty() {
		return arrayIndexedCollection.isEmpty();
	}
	
	/**
	 * Method returning stack size (number of elements currently on the stack).
	 * 
	 * @return stack size
	 */
	public int size() {
		return arrayIndexedCollection.size();
	}
	
	/**
	 * Method pushing (adding) the object on top of the stack.
	 * 
	 * @param value value to be pushed onto the stack
	 */
	public void push(Object value) {
		arrayIndexedCollection.add(value);
	} 
	
	/**
	 * Method popping (removing) the first element from the stack (the topmost element).
	 * 
	 * @return the popped value (element from the stack top)
	 * @throws EmptyStackException if stack is empty (no element to get from the stack)
	 */
	public Object pop() {
		if (isEmpty()) {
			throw new EmptyStackException("Stack is empty - no element to pop!");
		}
		int lastElemInd = size()-1;
		Object popObj = arrayIndexedCollection.get(lastElemInd);
		arrayIndexedCollection.remove(lastElemInd);
		
		return popObj;
	}
	
	/**
	 * Method peeking into the stack - getting the element from its top but not removing it from the stack.
	 * 
	 * @return the peeked value of element from the stack top
	 * @throws EmptyStackException if stack is empty (no element to get from the stack)
	 */
	public Object peek() {
		if (isEmpty()) {
			throw new EmptyStackException("Stack is empty - no element to peek!");
		}
		
		return arrayIndexedCollection.get(size()-1);
	}
	
	/**
	 * Method clearing the stack - removing all of its elements.
	 */
	public void clear() {
		while (!isEmpty()) {
			pop();
		}
	}

	/**
	 * Method giving hash representation of an <code>ObjectStack</code> object.
	 * 
	 * @return hash representation of an <code>ObjectStack</code> object
	 */
	@Override
	public int hashCode() {
		return Objects.hash(arrayIndexedCollection);
	}

	/**
	 * Method determining if two <code>ObjectStack</code> objects are equal: if their collections they are based on are equal.
	 * 
	 * @return true if the two objects are equal
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ObjectStack))
			return false;
		ObjectStack other = (ObjectStack) obj;
		return Objects.equals(arrayIndexedCollection, other.arrayIndexedCollection);
	}

	/**
	 * Method giving string representation of an <code>ObjectStack</code> object.
	 * 
	 * @return string representation of an <code>ObjectStack</code> object
	 */
	@Override
	public String toString() {
		return "ObjectStack is based on arrayIndexedCollection " + arrayIndexedCollection;
	}
}

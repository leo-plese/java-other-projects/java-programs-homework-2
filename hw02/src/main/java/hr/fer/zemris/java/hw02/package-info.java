/**
 * 
 * Package containing class representation of a complex number and possible operations on it through <code>ComplexNumber</code> class.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.hw02;
/**
 * 
 * Package containing <code>ComplexDemo</code> class used as a main program to test <code>ComplexNumber</code> complex number representation class from <code>hr.fer.zemris.java.hw02</code> package.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.hw02.demo;
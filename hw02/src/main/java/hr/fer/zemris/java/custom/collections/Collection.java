package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;

/**
 * 
 * Class <code>Collection/code> is a model of any kind of collection of objects.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public class Collection {

	/**
	 * default constructor of Collection (no input)
	 */
	public Collection() {
	}
	
	/**
	 * Method checking if a collection is empty (has no elements).
	 * 
	 * @return true if collection is empty
	 */
	protected boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * Method giving size of collection (number of objects stored in a collection). Here it returns 0, however, it should give a reasonable value specific to an implementation so it has to be correctly implemented there.
	 * 
	 * @return size of collection
	 */
	public int size() {
		return 0;
	}
	
	/**
	 * Method adding object to a collection. Here it does not do anything - it is specific to an implementation so it has to be correctly implemented there.
	 * 
	 * @param value object to be added to the collection
	 */
	public void add(Object value) {
	}
	
	/**
	 * Method checking if a value is contained in a collection. Here it returns false, however, it should give a reasonable value specific to an implementation so it has to be correctly implemented there.
	 * 
	 * @param value value to be checked upon in the collection
	 * @return true if the collection contains the given value
	 */
	public boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Method removing the object given as input if it exists. Here it returns false, however, it should give a reasonable value specific to an implementation so it has to be correctly implemented there.
	 * 
	 * @param value value to remove from the collection
	 * @return true if the object was removed from the collection
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * Method returning object array representation of the collection. Here it throws <code>UnsupportedOperationException</code> but it should be correctly implemented specific to the implementation.
	 * 
	 * @return array of objects contained in the collection
	 * @throws UnsupportedOperationException here it throws this exception but it should be correctly implemented specific to the implementation
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method performing an operation on each collection element. Here it is an empty method but it should be correctly implemented specific to the implementation.
	 * 
	 * @param processor <code>Processor</code> object for processing each collection element in the way it specifies 
	 */
	public void forEach(Processor processor) {
	}
	
	
	/**
	 * Method adding all of the elements from another collection to this. This other collection remains unchanged.
	 * 
	 * @param other other collection whose elements are to be added to this one
	 */
	public void addAll(final Collection other) {
		
		/**
		 * Class <code>LocalProcessor</code> is a processor which adds an element to this collection.
		 * 
		 * @author Leo
		 * @version 1.0
		 */
		class LocalProcessor extends Processor {
			
			/**
			 * default constructor - no input
			 */
			public LocalProcessor() {
			}
			
			/**
			 * Method processing input object by adding it to this collection.
			 */
			public void process(Object value) {
				add(value);
			}
		}
		
		other.forEach(new LocalProcessor());
	}
	
	/**
	 * Method clearing a collection (removing all of its elements). Here it does nothing but it should be correctly implemented specific to the implementation.
	 */
	public void clear() {
	}

	/**
	 * Method returning String representation of the <code>Collection</code>.
	 */
	@Override
	public String toString() {
		return "Collection [isEmpty()=" + isEmpty() + ", size()=" + size() + ", toArray()=" + Arrays.toString(toArray())
				+ "]";
	}
}

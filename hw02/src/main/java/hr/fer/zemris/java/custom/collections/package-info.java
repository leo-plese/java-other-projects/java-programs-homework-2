/**
 * 
 * Package contains class representations of collections modeled by <code>Collection</code> class some of whose implementations are e.g. <code>ArrayIndexedCollection</code> and <code>LinkedListIndexedCollection</code>. Alongside with these classes, there is <code>Processor</code> which enables 
 * general processing of collection data.
 * Also, the package contains representation of a general stack structure working with objects <code>ObjectStack</code> and for its purpose exists <code>EmptyStackException</code> which is thrown by it in appropriate cases.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.collections;
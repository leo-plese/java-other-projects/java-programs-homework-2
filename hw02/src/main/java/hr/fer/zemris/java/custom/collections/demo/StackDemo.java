package hr.fer.zemris.java.custom.collections.demo;

import java.util.Arrays;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * 
 * Class <code>StackDemo</code> is a main application enabling work with postfix numerical operations using <code>ObjectStack</code> object stack model.
 * The program accepts one command line input argument representing a postfix expression which is then properly parsed with help of an object stack and finally evaluating the expression and presenting it to the user onto the screen.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public class StackDemo {
	
	/**
	 * Main method to the class accepting one command line input argument representing a postfix expression.
	 * Also, it calls various checks to make sure the input is really a proper postfix expression, and if it is, it is evaluated and printed out onto the screen.
	 * 
	 * @param args one command line input argument representing a postfix expression.
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("One command line argument must be given!");
			return;
		}
		
		String[] inputStrings = getInputChars(args);
		if (inputStrings == null) {
			System.out.println("No operands or operators given!");
			return;
		}
		
		ObjectStack objStack = new ObjectStack();
		try {
			System.out.println("Expression evaluates to " + performOperations(inputStrings, objStack) + ".");
		} catch (UnsupportedOperationException | IllegalArgumentException ex) {
			System.out.println("MESSAGE: " + ex.getMessage());
		}
	}
	
	/**
	 * Method parsing the input of postfix expression into individual strings (operators and operands).
	 * 
	 * @param strings postfix expression to be parsed
	 * @return strings (operators +/- and operands) - null if the given string is invalid
	 */
	private static String[] getInputChars(String[] strings) {
		String[] splitInputs = strings[0].split("\\s+");

		if (splitInputs.length == 0 || (splitInputs.length == 1 && splitInputs[0].isEmpty())) {
			return null;
		}
		
		String[] inputs = splitInputs[0].isEmpty() ? Arrays.copyOfRange(splitInputs, 1, splitInputs.length) : splitInputs;
		
		return inputs;
	}
	
	/**
	 * Method performing operations on the operands (according to postfix notation) using object stack.
	 * 
	 * @param inputStrings operators and operands order in postfix order
	 * @param objStack stack used to manipulate operations
	 * @return final result of the operation
	 * @throws IllegalArgumentException if an operator was given other than supported or if the given expression is invalid (not proper postfix)
	 * @throws UnsupportedOperationException if an operation which is impossible to do is tried to be executed e.g. dividing (/) or remainder (%) by 0 
	 */
	private static int performOperations(String[] inputStrings, ObjectStack objStack) {
		for (String inStr : inputStrings) {
			try {
				int operand = Integer.valueOf(inStr);
				objStack.push(operand);
			} catch (NumberFormatException numberFormatEx) {
				final String operator = inStr;
				
				if (!isOperatorSupported(operator, getSupportedStackOperatorsStringArray())) {
					throw new IllegalArgumentException("Operator " + operator + " is not supported!");
				}
				final int operand1, operand2;
				
				try {
					operand2 = (Integer) objStack.pop();
					operand1 = (Integer) objStack.pop();
				} catch (EmptyStackException emptyStackEx) {
					throw new IllegalArgumentException("Given expression is invalid!");
				}
				
				if (operand2 == 0) {
					String errorMessage = "";
					switch (operator) {
						case "/":
							errorMessage = "Operation \"/\" with second operator equal to 0 is undefined!";
							break;
						case "%":
							errorMessage = "Operation \"%\" with second operator equal to 0 is undefined!";
							break;
					}
					if (!errorMessage.isEmpty()) {
						throw new UnsupportedOperationException(errorMessage);
					}
				}
				
		
				int result = getOperationResult(getSupportedStackOperatorsStringArray(), operator, operand1, operand2);
				objStack.push(result);
			}
		}
		
		if (objStack.size() != 1) {
			throw new IllegalArgumentException("Given expression is invalid!");
		}
		
		return (Integer) objStack.pop();
	}
	
	/**
	 * Method evaluating the final operation result according to the given operator.
	 * 
	 * @param stackOperators supported operators
	 * @param operator given operator to operate on the given operands
	 * @param operand1 first operand to the operation
	 * @param operand2 second operand to the operation
	 * @return result of the operation
	 */
	private static int getOperationResult(String[] stackOperators, String operator, int operand1, int operand2) {
		int result;
		if (operator.equals(stackOperators[0])) {
			result = operand1 + operand2;
		} else if (operator.equals(stackOperators[1])) {
			result = operand1 - operand2;
		} else if (operator.equals(stackOperators[2])) {
			result = operand1 / operand2;
		} else if (operator.equals(stackOperators[3])) {
			result = operand1 * operand2;
		} else {
			result = operand1 % operand2;
		}
		return result;
	}

	/**
	 * Method checking if a specific operator is on a supported list.
	 * 
	 * @param operator specific operator being checked upon
	 * @param supportedOperators all currently supported operators
	 * @return true if the operator is supported, false otherwise
	 */
	private static boolean isOperatorSupported(String operator, String[] supportedOperators) {
		for (String op : supportedOperators) {
			if (op.equals(operator)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Method getting array of supported operators in postfix operations represented by their String values.
	 * 
	 * @return array of supported operators in postfix operations.
	 */
	private static String[] getSupportedStackOperatorsStringArray() {
		StackOperator[] stackOperators = getSupportedStackOperatorsArray();
		int stackOperatorsNum = stackOperators.length;
		String[] operatorStringValues = new String[stackOperatorsNum];
		for (int i = 0; i < stackOperatorsNum; i++) {
			operatorStringValues[i] = stackOperators[i].getOperator();
		}
		
		return operatorStringValues;
	}
	
	/**
	 * Method getting array of supported operators in postfix operations represented by their <code>StackOperator</code> objects.
	 * 
	 * @return array of supported operator as <code>StackOperator</code> objects
	 */
	private static StackOperator[] getSupportedStackOperatorsArray() {
		return StackOperator.values();
	}
	
	
	/**
	 * 
	 * Enum <code>StackOperator</code> models a list of currently supported operators. It has to be changed as the support of different operators is changed.
	 * 
	 * @author Leo
	 * @version 1.0
	 * 
	 */
	private enum StackOperator {
		/**
		 * currently supported 5 operations: sum, difference, division, multiplication, remainder in integer division
		 */
		SUM("+"),
		DIFFERENCE("-"),
		DIVISION("/"),
		MULTIPLICATION("*"),
		REMAINDER("%");
		
		/**
		 * operator String value e.g. "+" for sum
		 */
		private final String operator;
		
		/**
		 * constructor attributing String value to the class field
		 * @param operator
		 */
		StackOperator(String operator) {
			this.operator = operator;
		}
		
		/**
		 * Method getting the operator.
		 * 
		 * @return operator
		 */
		public String getOperator() {
			return operator;
		}
	}
	
	
	


}

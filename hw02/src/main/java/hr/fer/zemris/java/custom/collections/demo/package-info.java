/**
 * 
 * Package containing <code>StackDemo</code> class used as a main program to test <code>ObjectStack</code> stack representation class from <code>hr.fer.zemris.java.custom.collections</code> package.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.collections.demo;
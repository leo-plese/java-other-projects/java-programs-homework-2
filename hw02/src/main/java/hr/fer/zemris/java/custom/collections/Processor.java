package hr.fer.zemris.java.custom.collections;


/**
 * 
 * Class <code>Processor</code> is a model of an object enabling performing an operation an a given object.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public class Processor {
	
	/**
	 * Method processing given object value input in a way (here empty method - it has to be implemented specifically to a given case).
	 * @param value given object given for processing
	 */
	public void process(Object value) {
	}
}

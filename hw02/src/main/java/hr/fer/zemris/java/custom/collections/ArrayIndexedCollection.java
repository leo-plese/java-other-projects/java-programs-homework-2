package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.Objects;

/**
 * 
 * Class <code>ArrayIndexedCollection</code> is a specific implementation of <code>Collection</code>.
 * It is based on an internal objects array storing its size (number of currently stored elements) and capacity (object array length).
 * 
 * @author Leo
 * @version 1.0
 */
public class ArrayIndexedCollection extends Collection {
	
	/**
	 * default capacity (used to set initial object array length)
	 */
	private static final int CAPACITY_DEFAULT = 16;
	/**
	 * object array length (maximum possible number of elements which can be stored in the collection)
	 */
	private static int capacity = CAPACITY_DEFAULT;
	/**
	 * collection size - number of currently stored elements in the array
	 */
	private int size;
	/**
	 * array of objects contained in collection whose length is initially set to default capacity
	 */
	private Object[] elements = new Object[CAPACITY_DEFAULT];
	
	
	/**
	 * default constructor - calling next constructor with default capacity <code>CAPACITY_DEFAULT</code>
	 */
	public ArrayIndexedCollection() {
		this(CAPACITY_DEFAULT);
	}
	
	/**
	 * constructor with custom initial capacity input
	 * 
	 * @param initialCapacity capacity the object array length is initially set to
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		this(null, false, initialCapacity);
	}
	
	/**
	 * constructor with given another collection based on which this one is going to be built
	 * 
	 * @param other other collection whose elements are to be added to this one
	 */
	public ArrayIndexedCollection(Collection other) {
		this(other, other.size());
	}
	
	/**
	 * constructor with given another collection based on which this one is going to be built and with given initial capacity of the objects array
	 * 
	 * @param other other collection whose elements are to be added to this one
	 * @param initialCapacity capacity the object array length is initially set to
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		this(other, true, initialCapacity);
	}
	
	/**
	 * private constructor doing the final job of objects array capacity and contained elements initialization
	 * 
	 * @param other other collection whose elements are to be added to this one
	 * @param otherCollectionNullCheck true if the collection is being built based on another one, false otherwise
	 * @param initialCapacity capacity the object array length is initially set to
	 * @throws IllegalArgumentException if collection initial is set to value which is not positive
	 */
	private ArrayIndexedCollection(Collection other, boolean otherCollectionNullCheck, int initialCapacity) {
		if (initialCapacity < 1) {
			throw new IllegalArgumentException("Collection capacity must be greater than or equal to 1!");
		}
		
		if (otherCollectionNullCheck) {
			Collection otherCol = Objects.requireNonNull(other, "Given collection must not be null!");
			int otherColSize = otherCol.size();
			capacity = initialCapacity < otherColSize ? otherColSize : initialCapacity;
			this.addAll(other);
		} else {
			capacity = initialCapacity;
		}
		
		elements =  Arrays.copyOf(elements, capacity);
	}
	
	/**
	 * Method returning collection size - number of elements stored in the object array.
	 * 
	 * @return size collection size
	 */
	@Override
	public int size() {
		return size;
	}
	
	/**
	 * Method adding an element to the collection, calling <code>insert</code> method which adds it to as the last element to the objects array.
	 * 
	 * @param value value to be added to the object array
	 */
	@Override
	public void add(Object value) {
		insert(value, size);
	}
	
	/**
	 * Method checking if an element is contained in the array.
	 * 
	 * @param value value to check if it is contained in the object array
	 * @return true if an element is contained in the collection, false otherwise
	 */
	@Override
	public boolean contains(Object value) {
		if (value == null) {
			return false;
		}
		
		for (Object obj : elements) {
			if (obj == null) {
				break;
			}
			if (obj.equals(value)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Method removing given object value from the collection - object array.
	 * 
	 * @param value to be removed from the object array
	 * @return true if the object was actually removed, false otherwise
	 */
	@Override
	public boolean remove(Object value) {
		int indexRemove = indexOf(value);
		
		if (indexRemove == -1) {
			return false;
		}
		
		remove(indexRemove);
		return true;
	}
	
	/**
	 * Method returning new array of objects with same elements as the current objects array.
	 * 
	 * @return array representation of the elements stored in the collection
	 */
	@Override
	public Object[] toArray() {
		Object[] newArr = new Object[size];
		for (int i = 0; i < size; i++) {
			newArr[i] = elements[i];
		}
		return newArr;
	}
	
	/**
	 * Method performing an operation specified by processor given as the input on each element of the collection.
	 * 
	 * @param processor processor processing elements of the collection in a way specified by it
	 */
	@Override
	public void forEach(Processor processor) {
		for (Object obj : elements) {
			if (obj == null) {
				return;
			}
			processor.process(obj);
		}
	}
	
	/**
	 * Method clearing the collection by removing elements from the object array.
	 */
	@Override
	public void clear() {
		for (int i = size-1; i >= 0; i--) {
			remove(i);
		}
	}
	
	/**
	 * Method getting object value from the specified index from the object array.
	 * 
	 * @param index index from the object array to get the value from
	 * @return object gotten as the element of the object array at the specified index
	 * @throws IndexOutOfBoundsException if index given as the input is out of range of the object array indexes (from 0 to collection size - 1)
	 */
	public Object get(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException("Index must be greater than or equal to 0 and less than collection size!");
		}
		return elements[index];
	}
	
	/**
	 * Method inserting a value into the object array to specified index.
	 * 
	 * @param value value to be inserted into specified position (index to the object array)
	 * @param position index to the object array where the given value is to be inserted
	 * @throws IndexOutOfBoundsException if index given as the input is out of range of the object array indexes increased by 1 (from 0 to collection size)
	 * 
	 * Notice:
	 * -the index=size is permitted but in that case the array has to be reallocated (doubled in its capacity) at first
	 * -value <code>null</code> is not allowed to insert into the collection 
	 */
	public void insert(Object value, int position) {
		Object valToAdd = Objects.requireNonNull(value);

		if (position < 0 || position > size) {
			throw new IndexOutOfBoundsException("Position for insert must be greater than or equal to 0 and less than or equal to collection size!");
		}
		
		if (size == capacity) {
			capacity *= 2;
			elements = Arrays.copyOf(elements, capacity);
		}
		
		for (int i = size-1; i >= position; i--) {
			elements[i+1] = elements[i];
		}
		
		elements[position] = valToAdd;
		size++;
	}
	
	/**
	 * Method getting index of the first occurrence of a given object in the object array.
	 * 
	 * Notice: returns -1 if value is null (unallowed value) or if it is not contained in the object array.
	 * 
	 * @param value value whose index in the array is being looked for
	 * @return the index of the first occurrence of a given object in the object array
	 */
	public int indexOf(Object value) {
		if (value == null || !contains(value)) {
			return -1;
		}
		
		int indexFound = -1;
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				indexFound = i;
				break;
			}
		}
		
		return indexFound;
	}
	
	/**
	 * Method removing element at specified index in the object array.
	 * 
	 * @param index index the element from the array is being removed from
	 * @throws IndexOutOfBoundsException if index is out of range of the object array indexes (from 0 to collection size - 1)
	 */
	public void remove(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException("Index must be greater than or equal to 0 and less than collection size!");
		}
		
		for (int i = index+1; i < size; i++) {
			elements[i-1] = elements[i];
		}
		
		size--;
		elements[size] = null;
	}

	/**
	 * Method getting capacity (current object array length).
	 * 
	 * @return object array capacity
	 */
	public static int getCapacity() {
		return capacity;
	}

	/**
	 * Method giving hash representation of an <code>ArrayIndexedCollection</code> object.
	 * 
	 * @return hash representation of an <code>ArrayIndexedCollection</code> object
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Objects.hash(size);
		result = prime * result + Arrays.deepHashCode(elements);
		return result;
	}

	/**
	 * Method determining if two <code>ArrayIndexedCollection</code> objects are equal: if their object arrays and their sizes are equal.
	 * 
	 * @return true if the two objects are equal
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ArrayIndexedCollection))
			return false;
		ArrayIndexedCollection other = (ArrayIndexedCollection) obj;
		return Arrays.deepEquals(elements, other.elements) && size == other.size;
	}

	/**
	 * Method giving string representation of an <code>ArrayIndexedCollection</code> object.
	 * 
	 * @return string representation of an <code>ArrayIndexedCollection</code> object
	 */
	@Override
	public String toString() {
		return "ArrayIndexedCollection has size of " + size + "and its elements array is " + Arrays.toString(elements);
	}
}

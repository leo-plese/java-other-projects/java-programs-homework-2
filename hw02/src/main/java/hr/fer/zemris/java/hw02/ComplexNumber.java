package hr.fer.zemris.java.hw02;

import static java.lang.Math.*;

import java.util.Objects;

/**
 * Class <code>ComplexNumber</code> representing mathematical model of a complex number with appropriate operations.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public final class ComplexNumber {

	/**
	 * real part of a complex number
	 */
	private double real;
	/**
	 * imaginary part of a complex number
	 */
	private double imaginary;

	/**
	 * constructor given real and imaginary part of a complex number - two components a complex number consists of
	 * 
	 * @param real real part of a complex number
	 * @param imaginary imaginary part of a complex number
	 * @throws NullPointerException if either real or imaginary part input is <code>null</code>
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = Objects.requireNonNull(real, "Real part must not be null!");
		this.imaginary = Objects.requireNonNull(imaginary, "Imaginary part must not be null!");
	}

	/**
	 * Method making a complex number from its real component (imaginary is 0).
	 * 
	 * @param real real part of a complex number to build a complex number from
	 * @return complex number built from real component
	 * @throws NullPointerException if given real part input is <code>null</code>
	 */
	public static ComplexNumber fromReal(double real) {
		real = Objects.requireNonNull(real, "Real part must not be null!");
		
		return new ComplexNumber(real, 0);
	}

	/**
	 * Method making a complex number from its imaginary component (real is 0).
	 * 
	 * @param imaginary imaginary part of a complex number to build a complex number from
	 * @return complex number built from imaginary component
	 * @throws NullPointerException if given imaginary part input is <code>null</code>
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		imaginary = Objects.requireNonNull(imaginary, "Imaginary part must not be null!");
		
		return new ComplexNumber(0, imaginary);
	}

	/**
	 * Method making a complex number from its polar components (magnitude and angle).
	 * 
	 * @param magnitude magnitude a complex number
	 * @param angle angle of a complex number
	 * @return complex number from given polar components
	 * @throws NullPointerException if either magnitude or angle input is <code>null</code>
	 * @throws IllegalArgumentException if given magnitude is negative (unallowed)
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		magnitude = Objects.requireNonNull(magnitude, "Magnitude must not be null!");
		angle = Objects.requireNonNull(angle, "Angle must not be null!");
		
		if (magnitude < 0) {
			throw new IllegalArgumentException("Complex number magnitude must be a nonnegative number!");
		}
		
		return new ComplexNumber(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}

	/**
	 * Method parsing String input into a complex number <code>ComplexNumber</code> object.
	 * 
	 * @param s string being parsed into a complex number
	 * @return complex number gotten from the String input
	 * @throws NullPointerException if given String input is <code>null</code>
	 * @throws IllegalArgumentException if given String input is of an unallowed format e.g. it has a number in scientific notation, more plus/minus signs in a row, it is empty and other reasons because of which either real or imaginary part cannot be parsed 
	 */
	public static ComplexNumber parse(String s) {
		s = Objects.requireNonNull(s, "String for parsing into complex number must not be null!");

		if (s.trim().isEmpty()) {
			throw new IllegalArgumentException("Empty string cannot be parsed!");
		}

		if (s.contains("E") || s.contains("e")) {
			throw new IllegalArgumentException("Scientific notation not supported!");
		}

		if (entryContainsPlusesMinusesInARow(s)) {
			throw new IllegalArgumentException("Entry contains more pluses/minuses in a row!");
		}

		s = s.trim();

		if (!s.contains("i")) {
			try {
				double real = Double.parseDouble(s.charAt(0) + s.substring(1).trim());
				return new ComplexNumber(real, 0);
			} catch (NumberFormatException ex) {
				throw new IllegalArgumentException("Real part could not be parsed!");
			}
		}

		// number contains i -> pure imaginary or normal complex number (both must have
		// "i" as the last character)
		if (s.charAt(s.length() - 1) != 'i') {
			throw new IllegalArgumentException("Imaginary part could not be parsed!");
		}

		int plusLastInd = s.lastIndexOf('+');
		int minusLastInd = s.lastIndexOf('-');
		int signLastInd = plusLastInd > minusLastInd ? plusLastInd : minusLastInd;

		if (signLastInd == -1) {
			try {
				ComplexNumber c = parsePureImaginary(s);
				return c;
			} catch (NumberFormatException ex) {
				throw ex;
			}
		}

		double real;
		if (s.substring(0, signLastInd).isEmpty()) {
			real = 0;
		} else {
			try {
				String sRe = s.substring(0, signLastInd);
				real = Double.parseDouble(sRe.charAt(0) + sRe.substring(1).trim());
			} catch (NumberFormatException ex) {
				throw new IllegalArgumentException("Real part could not be parsed!");
			}
		}

		double imaginary;
		try {
			String sIm = s.substring(signLastInd, s.lastIndexOf('i'));
			if (sIm.substring(1).isBlank()) {
				sIm = sIm.charAt(0) + "1";
			}
			imaginary = Double.parseDouble(sIm.charAt(0) + sIm.substring(1).trim());
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException("Imaginary part could not be parsed!");
		}

		return new ComplexNumber(real, imaginary);
	}
	
	/**
	 * Method called in <code>parse</code> to parse a pure imaginary number.
	 * 
	 * @param c String representation of the pure imaginary number
	 * @return complex number built from the String representation
	 * @throws IllegalArgumentException if imaginary part could not be parsed
	 */
	private static ComplexNumber parsePureImaginary(String c) {
		try {
			String sIm = c.substring(0, c.lastIndexOf('i'));
			if (sIm.isEmpty()) {
				sIm = "1";
			}
			double imaginary = Double.parseDouble(sIm);
			return new ComplexNumber(0, imaginary);
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException("Imaginary part could not be parsed!");
		}
	}
	/**
	 * Method called in <code>parse</code> to check if there are multiple signs + or - in a row.
	 * 
	 * @param s String input for parsing
	 * @return true if the input contains multiple signs + or - in a row, false otherwise
	 */

	private static boolean entryContainsPlusesMinusesInARow(String s) {
		char[] chars = s.replaceAll("\\s+", "").toCharArray();
		for (int i = 0, n = chars.length; i < n; i++) {
			char c = chars[i];
			int signsInARow = 0;

			while (c == '+' || c == '-') {
				signsInARow++;
				if (signsInARow == 2) {
					return true;
				}
				i++;
				c = chars[i];
			}
		}
		return false;
	}

	/**
	 * Method getting real part of a complex number.
	 * 
	 * @return real part of a complex number
	 */
	public double getReal() {
		return real;
	}

	/**
	 * Method getting imaginary part of a complex number.
	 * 
	 * @return imaginary part of a complex number
	 */
	public double getImaginary() {
		return imaginary;
	}

	/**
	 * Method getting magnitude of a complex number.
	 * 
	 * @return magnitude of a complex number
	 */
	public double getMagnitude() {
		if (real != 0 && imaginary == 0) {
			return abs(real);
		} else if (real == 0 && imaginary != 0) {
			return abs(imaginary);
		}
		return sqrt(real * real + imaginary * imaginary);
	}

	/**
	 * Method getting angle of a complex number.
	 * 
	 * @return angle of a complex number
	 */
	public double getAngle() {

		return real >= 0 ? atan(imaginary / real) : PI + atan(imaginary / real);
	}

	/**
	 * Method adding two complex numbers.
	 * 
	 * @param c complex number to be added to this one
	 * @return sum of the complex numbers
	 * @throws NullPointerException if given number is <code>null</code>
	 */
	public ComplexNumber add(ComplexNumber c) {
		c = Objects.requireNonNull(c, "Complex number for addition must not be null!");
		
		return new ComplexNumber(real + c.getReal(), imaginary + c.getImaginary());
	}

	/**
	 * Method subtracting two complex numbers.
	 * 
	 * @param c complex number to be subtracted from this one
	 * @return difference of the complex numbers
	 * @throws NullPointerException if given number is <code>null</code>
	 */
	public ComplexNumber sub(ComplexNumber c) {
		c = Objects.requireNonNull(c, "Complex number for subtraction must not be null!");
		
		return new ComplexNumber(real - c.getReal(), imaginary - c.getImaginary());
	}

	/**
	 * Method multiplying two complex numbers.
	 * 
	 * @param c complex number to be multiplied by this one
	 * @return product of the complex numbers
	 * @throws NullPointerException if given number is <code>null</code>
	 */
	public ComplexNumber mul(ComplexNumber c) {
		c = Objects.requireNonNull(c, "Complex number for multiplication must not be null!");
		
		double cRe = c.getReal(), cIm = c.getImaginary();
		return new ComplexNumber(real * cRe - imaginary * cIm, real * cIm + imaginary * cRe);
	}

	/**
	 * Method dividing two complex numbers.
	 * 
	 * @param c complex number this one is divided by
	 * @return quotient of the complex numbers
	 * @throws NullPointerException if given number is <code>null</code>
	 */
	public ComplexNumber div(ComplexNumber c) {
		c = Objects.requireNonNull(c, "Complex number for division must not be null!");
		
		double cRe = c.getReal(), cIm = c.getImaginary();
		if (cRe == 0 && cIm == 0) {
			throw new IllegalArgumentException("Cannot divide by 0! (Result would be NaN)");
		}
		ComplexNumber product = mul(new ComplexNumber(cRe, -cIm));
		double productRe = product.getReal(), productIm = product.getImaginary();
		double divisor = cRe * cRe + cIm * cIm;
		return new ComplexNumber(productRe / divisor, productIm / divisor);
	}

	/**
	 * Method calculating power of a complex number.
	 * 
	 * @param n exponent of a complex number
	 * @return complex number to the given power
	 * @throws NullPointerException if given number is <code>null</code>
	 * @throws IllegalArgumentException if given number is negative (unallowed)
	 */
	public ComplexNumber power(int n) {
		n = Objects.requireNonNull(n, "Power number must not be null!");
		
		if (n < 0) {
			throw new IllegalArgumentException("Power must be a nonnegative integer!");
		}
		
		if (real == 0 && imaginary == 0) {
			return new ComplexNumber(0, 0);
		}
		
		if (n == 0) {
			return new ComplexNumber(1, 0);
		} else if (n == 1) {
			return this;
		}

		double magnitudeToNthPower = pow(getMagnitude(), n);
		double angleNthMultiple = getAngle() * n;
		
		double sin = sin(angleNthMultiple), cos = cos(angleNthMultiple);
		double sinCalc = abs(sin) < 1E-15 ? 0 : sin;
		double cosCalc = abs(cos) < 1E-15 ? 0 : cos;
		
		return new ComplexNumber(magnitudeToNthPower * cosCalc,
				magnitudeToNthPower * sinCalc);
	}

	/**
	 * Method calculating roots of a complex number.
	 * 
	 * @param n nth root of a complex number
	 * @return roots array of a complex number
	 * @throws NullPointerException if given number is <code>null</code>
	 * @throws IllegalArgumentException if given number is negative or 0 (unallowed)
	 */
	public ComplexNumber[] root(int n) {
		n = Objects.requireNonNull(n, "Root number must not be null!");
		
		if (n <= 0) {
			throw new IllegalArgumentException("Power must be a positive integer!");
		}

		if (n == 1) {
			return new ComplexNumber[] { this };
		}

		ComplexNumber[] complexRoots = new ComplexNumber[n];

		if (real == 0 && imaginary == 0) {
			for (int i = 0; i < n; i++) {
				complexRoots[i] = new ComplexNumber(0, 0);
			}
			return complexRoots;
		}

		double magnitudeNthRoot = pow(getMagnitude(), 1.0 / n);
		double angle = getAngle();

		for (int k = 0; k < n; k++) {
			double angleMultiple = (angle + 2 * k * PI) / n;
			double sin = sin(angleMultiple), cos = cos(angleMultiple);
			double sinCalc = abs(sin) < 1E-15 ? 0 : sin;
			double cosCalc = abs(cos) < 1E-15 ? 0 : cos;
			complexRoots[k] = new ComplexNumber(magnitudeNthRoot * cosCalc, magnitudeNthRoot * sinCalc);
		}

		return complexRoots;
	}

	/**
	 * Method giving string representation of an <code>ComplexNumber</code> object.
	 * 
	 * @return string representation of an <code>ComplexNumber</code> object
	 */
	@Override
	public String toString() {

		if (real == 0 && imaginary == 0) {
			return "0";
		}

		String imString = getImaginaryPartAsString(imaginary);

		if (real != 0 && imaginary != 0) {
			return real + (signum(imaginary) == 1.0 ? "+" : "") + imString + "i";
		} else if (real != 0) {
			return "" + real;
		} else {
			return imString + "i";
		}
	}

	/**
	 * Method getting string representation of a complex number's imaginary part.
	 * 
	 * Notice: if absolute value of imaginary part is 1, no numerical value is
	 * included in the string - only "" or "-" if it is +1 or -1 respectively.
	 * Otherwise, retrieves string value of double value of the imaginary part.
	 * 
	 * Important: By using "==" precision is limited to 1E-15 i.e. 1.000000000000001
	 * is still different from 1 but 1.0000000000000001 is equal. If greater
	 * precision is needed, abs(im-1.0) < epsilon pattern (epsilon is const. value)
	 * can be used.
	 * 
	 * @param im imaginary part of complex number
	 * @return empty string/"-"/string value of imaginary part - if imaginary part
	 *         is equal to 1/-1/other value - respectively
	 */
	private static String getImaginaryPartAsString(double im) {
		String imString;

		if (im == 1) {
			imString = "";
		} else if (im == -1) {
			imString = "-";
		} else {
			imString = String.valueOf(im);
		}

		return imString;
	}

	/**
	 * Method giving hash representation of an <code>ComplexNumber</code> object.
	 * 
	 * @return hash representation of an <code>ComplexNumber</code> object
	 */
	@Override
	public int hashCode() {
		return Objects.hash(imaginary, real);
	}

	/**
	 * Method determining if two <code>ComplexNumber</code> objects are equal: if their real and imaginary parts are equal.
	 * 
	 * @return true if the two objects are equal
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ComplexNumber))
			return false;
		ComplexNumber other = (ComplexNumber) obj;
		return Double.doubleToLongBits(imaginary) == Double.doubleToLongBits(other.imaginary)
				&& Double.doubleToLongBits(real) == Double.doubleToLongBits(other.real);
	}

	
	


}

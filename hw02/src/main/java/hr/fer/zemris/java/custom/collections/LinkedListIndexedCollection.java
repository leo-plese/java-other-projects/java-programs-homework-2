package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * 
 * Class <code>LinkedListIndexedCollection</code> is a specific implementation of <code>Collection</code>.
 * It is based on an internal linked list structure storing its size (number of currently stored elements) and first and last element in the list.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public class LinkedListIndexedCollection extends Collection {

	/**
	 * first element of the list
	 */
	private ListNode first;
	/**
	 * last element of the list
	 */
	private ListNode last;
	/**
	 * size of the collection - number of currently stored elements in the list
	 */
	private int size;

	/**
	 * Class <code>ListNode</code> is a private structure representing a model of a list element containing the information about the previous and next element as well as its value which is of Object type.
	 * 
	 * @author Leo
	 * @version 1.0
	 */
	private static class ListNode {
		/**
		 * previous element - element coming before the node
		 */
		private ListNode previous;
		/**
		 * next element - element coming after the node
		 */
		private ListNode next;
		/**
		 * value stored in the element
		 */
		Object value;
	}

	/**
	 * default constructor - initializing first and last list node to null
	 */
	public LinkedListIndexedCollection() {
		first = null;
		last = null;
	}

	/**
	 * constructor with given another collection based on which this one is going to be built
	 * 
	 * @param collection other collection whose elements are to be added to this one
	 */
	public LinkedListIndexedCollection(Collection collection) {
		addAll(collection);
	}

	/**
	 * Method returning collection size - number of elements stored in the list.
	 * 
	 * @return collection size
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Method adding an element to the collection, calling <code>insert</code> method which adds it to as the last element to the list.
	 * 
	 * @param value value to be added to the list
	 */
	@Override
	public void add(Object value) {
		insert(value, size);
	}

	/**
	 * Method checking if an element is contained in the list.
	 * 
	 * @param value value to check if it is contained in the list
	 * @return true if an element is contained in the collection, false otherwise
	 */
	@Override
	public boolean contains(Object value) {
		if (value == null) {
			return false;
		}

		ListNode node = first;
		while (node != null) {
			if (node.value.equals(value)) {
				return true;
			}
			node = node.next;
		}

		return false;
	}

	/**
	 * Method removing given object value from the collection - list.
	 * 
	 * @param value to be removed from the list
	 * @return true if the object was actually removed, false otherwise
	 */
	@Override
	public boolean remove(Object value) {
		int indexRemove = indexOf(value);

		if (indexRemove == -1) {
			return false;
		}

		remove(indexRemove);
		return true;
	}

	/**
	 * Method returning new array of objects with same elements as the current list.
	 * 
	 * @return array representation of the elements stored in the collection
	 */
	@Override
	public Object[] toArray() {
		Object[] newArr = new Object[size];

		ListNode node = first;
		for (int i = 0; node != null; i++) {
			System.out.println("i=" + i + ", val="+node.value);
			newArr[i] = node.value;
			node = node.next;
		}

		return newArr;
	}

	/**
	 * Method performing an operation specified by processor given as the input on each element of the collection.
	 * 
	 * @param processor processor processing elements of the collection in a way specified by it
	 */
	@Override
	public void forEach(Processor processor) {
		ListNode node = first;
		while (node != null) {
			processor.process(node.value);
			node = node.next;
		}
	}

	/**
	 * Method clearing the collection by removing elements from the list.
	 */
	@Override
	public void clear() {
		for (int i = size-1; i >= 0; i--) {
			System.out.println(i);
			System.out.println("s=" +size);
			remove(i);
		}
	}

	/**
	 * Method getting object value from the specified index from the list.
	 * 
	 * @param index index from the list to get the value from the list
	 * @return object gotten as the element of the list at the specified index
	 * @throws IndexOutOfBoundsException if index given as the input is out of allowed range (from 0 to collection size - 1)
	 */
	public Object get(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException(
					"Index must be greater than or equal to 0 and less than collection size!");
		}

		if (index <= size / 2) {
			ListNode node = first;
			for (int i = 0; i < index; i++) {
				node = node.next;
			}
			return node.value;
		} else {
			ListNode node = last;
			for (int i = size - 1; i > index; i--) {
				node = node.previous;
			}
			return node.value;
		}

	}

	/**
	 * Method inserting a value into the list to specified index.
	 * 
	 * @param value value to be inserted into specified position (index to the list)
	 * @param position index to the list where the given value is to be inserted
	 * @throws IndexOutOfBoundsException if index given as the input is out of range of the list size increased by 1 (from 0 to collection size)
	 * 
	 * Notice: value <code>null</code> is not allowed to insert into the collection 
	 */
	public void insert(Object value, int position) {
		Object valToAdd = Objects.requireNonNull(value);

		if (position < 0 || position > size) {
			throw new IndexOutOfBoundsException(
					"Position for insert must be greater than or equal to 0 and less than or equal to collection size!");
		}

		ListNode nodeAdd = new ListNode();
		nodeAdd.value = valToAdd;
		
		if (size == 0) {
			first = nodeAdd;
			last = nodeAdd;
			nodeAdd.previous = null;
			nodeAdd.next = null;
			size++;
			return;
		}

		if (position == size) {
			nodeAdd.next = null;
			nodeAdd.previous = last;
			
			last.next = nodeAdd;
			last = nodeAdd;
		} else if (position == 0) {
			nodeAdd.previous = null;
			nodeAdd.next = first;
			
			first.previous = nodeAdd;
			first = nodeAdd;
		} else {
			ListNode node;
			if (position <= size / 2) {
				node = first;
				for (int i = 0; i < position - 1; i++) {
					node = node.next;
				}
			} else {
				node = last;
				for (int i = size - 1; i >= position; i--) {
					node = node.previous;
				}
			}
			nodeAdd.next = node.next;
			nodeAdd.previous = node;
			
			node.next.previous = nodeAdd;
			node.next = nodeAdd;
		}

		size++;
	}

	/**
	 * Method getting index of the first occurrence of a given object in the list.
	 * 
	 * Notice: returns -1 if value is null (unallowed value) or if it is not contained in the list.
	 * 
	 * @param value value whose index in the list is being looked for
	 * @return the index of the first occurrence of a given object in the list
	 */
	public int indexOf(Object value) {
		if (value == null || !contains(value)) {
			return -1;
		}

		int indexFound = -1;

		ListNode node = first;
		for (int i = 0; node != null; i++) {
			if (node.value.equals(value)) {
				indexFound = i;
				break;
			}
			node = node.next;
		}

		return indexFound;
	}

	/**
	 * Method removing element at specified index in the list.
	 * 
	 * @param index index the element from the list is being removed from
	 * @throws IndexOutOfBoundsException if index is out of allowed range (from 0 to collection size - 1)
	 */
	public void remove(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException(
					"Index must be greater than or equal to 0 and less than collection size!");
		}


		if (index == 0) {
			System.out.println("Here size " + size + " "+ first.next);
			
			if (size == 1) {
				first = null;
			} else {
				first.next.previous = null;
				ListNode oldNode = first;
				first = first.next;
				oldNode.next = null;
				oldNode = null;
			}
		} else if (index == size - 1) {
			System.out.println("TUUU" + last);
			ListNode oldNode = last;
			last = last.previous;
			last.next = null;
			oldNode.previous = null;
			oldNode = null;
			System.out.println("TUUU  22");
		} else {
			ListNode node;
			if (index <= size / 2) {
				node = first;
				for (int i = 0; i < index; i++) {
					node = node.next;
				}
			} else {
				node = last;
				for (int i = size - 1; i > index; i--) {
					node = node.previous;
				}
			}

			node.previous.next = node.next;
			node.next.previous = node.previous;
			node.previous = null;
			node.next = null;
			node = null;
		}

		size--;
	}
	
	/**
	 * Method getting the first element of the linked list.
	 * 
	 * @return first element of the linked list
	 */
	public ListNode getFirst() {
		return first;
	}

	/**
	 * Method getting the last element of the linked list.
	 * 
	 * @return last element of the linked list
	 */
	public ListNode getLast() {
		return last;
	}

	/**
	 * Method giving hash representation of an <code>LinkedListIndexedCollection</code> object.
	 * 
	 * @return hash representation of an <code>LinkedListIndexedCollection</code> object
	 */
	@Override
	public int hashCode() {
		return Objects.hash(first, last, size);
	}

	/**
	 * Method determining if two <code>LinkedListIndexedCollection</code> objects are equal: if their first and last node of their linked lists are equal and if their sizes are equal.
	 * 
	 * @return true if the two objects are equal
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LinkedListIndexedCollection))
			return false;
		LinkedListIndexedCollection other = (LinkedListIndexedCollection) obj;
		return Objects.equals(first, other.first) && Objects.equals(last, other.last) && size == other.size;
	}

	/**
	 * Method giving string representation of an <code>LinkedListIndexedCollection</code> object.
	 * 
	 * @return string representation of an <code>LinkedListIndexedCollection</code> object
	 */
	@Override
	public String toString() {
		return "LinkedListIndexedCollection has first element " + first + ", last element " + last + " and size of " + size;
	}
}

package hr.fer.zemris.java.custom.collections;

/**
 * Class <code>EmptyStackException</code> models an unchecked exception thrown when there is no element from the stack to get (e.g. pop or peek).
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public class EmptyStackException extends RuntimeException {

	/**
	 * identifier of the <code>EmptyStackException</code> class
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * default constructor - calling <code>RuntimeException</code> super constructor
	 */
	public EmptyStackException() {
		super();
	}
	
	/**
	 * constructor calling <code>RuntimeException</code> super constructor by forwarding the appropriate message given as the input
	 * 
	 * @param message exception information message
	 */
	public EmptyStackException(String message) {
		super(message);
	}
}

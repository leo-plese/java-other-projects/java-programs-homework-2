package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ArrayIndexedCollectionTest {

	@Test
	public void defaultConstructorTest() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(16, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
	}

	@Test
	public void initialCapacityTenConstructorTest() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection(10);
		assertEquals(10, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
	}

	@Test
	public void initialCapacityOneConstructorTest() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection(1);
		assertEquals(1, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
	}

	@Test
	public void initialCapacityZeroConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(0));
	}

	@Test
	public void initialCapacityNegativeOneConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(-1));
	}

	@Test
	public void otherCollectionOfSizeThreeConstructorTest() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection(prepareOtherCollection());
		assertEquals(3, ArrayIndexedCollection.getCapacity());
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void otherCollectionNullConstructorTest() {
		assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection(null));
	}

	@Test
	public void otherCollectionOfSizeThreeInitialCapacityTenConstructorTest() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection(prepareOtherCollection(), 10);
		assertEquals(10, ArrayIndexedCollection.getCapacity());
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void otherCollectionOfSizeTwoInitialCapacityOneConstructorTest() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection(prepareOtherCollection(), 1);
		assertEquals(3, ArrayIndexedCollection.getCapacity());
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void otherCollectionOfSizeTwoInitialCapacityZeroConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(prepareOtherCollection(), 0));
	}

	@Test
	public void otherCollectionNullInitialCapacityTenConstructorTest() {
		assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection(null, 10));
	}

	@Test
	public void otherCollectionNullInitialCapacityZeroConstructorTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(null, 0));
	}

	@Test
	public void collectionOfSizeThreeSize() {
		assertEquals(3, prepareOtherCollection().size());
	}

	@Test
	public void collectionOfSizeZeroSize() {
		assertEquals(0, new ArrayIndexedCollection().size());
	}

	@Test
	public void collectionThreeNonNullElementsWithDuplicatesAdd() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		arrayIndexedCollection.add(new String("A"));
		arrayIndexedCollection.add(new String("B"));
		arrayIndexedCollection.add(new String("A"));

		assertEquals(3, arrayIndexedCollection.size());
		assertEquals("A", arrayIndexedCollection.get(0));
		assertEquals("B", arrayIndexedCollection.get(1));
		assertEquals("A", arrayIndexedCollection.get(2));
	}

	@Test
	public void collectionNullElementAdd() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		arrayIndexedCollection.add(new String("A"));
		arrayIndexedCollection.add(new String("B"));
		assertThrows(NullPointerException.class, () -> arrayIndexedCollection.add(null));
	}

	@Test
	public void collectionThreeElementsContainsNonNull() {
		Collection arrayIndexedCollection = prepareOtherCollection();
		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
	}

	@Test
	public void collectionThreeElementsContainsNull() {
		Collection arrayIndexedCollection = prepareOtherCollection();
		assertFalse(arrayIndexedCollection.contains(null));
	}

	@Test
	public void collectionThreeElementsRemoveExisting() {
		Collection arrayIndexedCollection = prepareOtherCollection();

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertEquals(3, arrayIndexedCollection.size());

		assertTrue(arrayIndexedCollection.remove("C"));

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertFalse(arrayIndexedCollection.contains("C"));
		assertEquals(2, arrayIndexedCollection.size());
	}

	@Test
	public void collectionThreeElementsRemoveNonexisting() {
		Collection arrayIndexedCollection = prepareOtherCollection();

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertFalse(arrayIndexedCollection.contains("D"));
		assertEquals(3, arrayIndexedCollection.size());

		assertFalse(arrayIndexedCollection.remove("D"));

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertFalse(arrayIndexedCollection.contains("D"));
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void collectionThreeElementsRemoveNull() {
		Collection arrayIndexedCollection = prepareOtherCollection();

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertEquals(3, arrayIndexedCollection.size());

		assertFalse(arrayIndexedCollection.remove(null));

		assertTrue(arrayIndexedCollection.contains("A"));
		assertTrue(arrayIndexedCollection.contains("B"));
		assertTrue(arrayIndexedCollection.contains("C"));
		assertEquals(3, arrayIndexedCollection.size());
	}

	@Test
	public void emptyCollectionToArray() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		Object[] colArray = arrayIndexedCollection.toArray();

		assertEquals(0, colArray.length);
	}

	@Test
	public void threeElementsCollectionToArray() {
		Object[] colArray = prepareOtherCollection().toArray();

		assertEquals(3, colArray.length);
	}

	@Test
	public void emptyCollectionForEach() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		LocalProcessor processor = new LocalProcessor();
		arrayIndexedCollection.forEach(processor);
		
		assertEquals(0, processor.counter);
	}

	@Test
	public void threeElementsCollectionForEach() {
		Collection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());

		LocalProcessor processor = new LocalProcessor();
		arrayIndexedCollection.forEach(processor);
		
		assertEquals(3, processor.counter);
	}

	private static class LocalProcessor extends Processor {

		int counter;

		public LocalProcessor() {
		}

		public void process(Object value) {
			counter++;
		}
	}
	
	
	
	@Test
	public void emptyCollectionClear() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());

		arrayIndexedCollection.clear();
		
		assertEquals(0, arrayIndexedCollection.size());
	}
	
	@Test
	public void threeElementsCollectionClear() {
		Collection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());

		arrayIndexedCollection.clear();
		
		assertEquals(0, arrayIndexedCollection.size());
	}
	
	
	
	@Test
	public void emptyCollectionGetNegativeIndexElement() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(-1));
	}
	
	@Test
	public void emptyCollectionGetPositiveIndexElement() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(1));
	}
	
	@Test
	public void threeElementsCollectionGetNegativeIndexElement() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(-1));
	}
	
	@Test
	public void threeElementsCollectionGetPositiveOutOfSizeIndex() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.get(3));
	}
	
	
	@Test
	public void insertElementsIntoEmptyCollection() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		arrayIndexedCollection.insert("R", 0);
		arrayIndexedCollection.insert("Q", 1);
		
		assertEquals(2, arrayIndexedCollection.size());
	}
	
	@Test
	public void insertElementsIntoEmptyCollectionNegativeIndex() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", -1));
	}
	
	@Test
	public void insertElementsIntoEmptyCollectionPositiveOutOfSizeIndex() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", 1));
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollection() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		arrayIndexedCollection.insert("R", 3);
		arrayIndexedCollection.insert("Q", 4);

		assertEquals(5, arrayIndexedCollection.size());
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollectionNegativeIndex() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", -1));
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollectionPositiveOutOfSizeIndex() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.insert("R", 4));
	}
	
	
	
	@Test
	public void indexOfElementsInEmptyCollection() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertEquals(-1, arrayIndexedCollection.indexOf(4));
		assertEquals(-1, arrayIndexedCollection.indexOf(null));
	}
	
	@Test
	public void indexOfExistingElementsInThreeElementsCollection() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertEquals(0, arrayIndexedCollection.indexOf("A"));
		assertEquals(1, arrayIndexedCollection.indexOf("B"));
		assertEquals(2, arrayIndexedCollection.indexOf("C"));
	}
	
	@Test
	public void indexOfNonexistingElementsInThreeElementsCollection() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		assertEquals(-1, arrayIndexedCollection.indexOf(4));
		assertEquals(-1, arrayIndexedCollection.indexOf(null));
	}
	
	
	
	@Test
	public void removeAtIndexFromEmptyCollection() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(0, arrayIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> arrayIndexedCollection.remove(0));
	}
	
	@Test
	public void removeAtAllowedIndexFromThreeElementCollection() {
		ArrayIndexedCollection arrayIndexedCollection = prepareOtherCollection();
		assertEquals(3, arrayIndexedCollection.size());
		
		arrayIndexedCollection.remove(2);
		assertEquals(2, arrayIndexedCollection.size());
		arrayIndexedCollection.remove(1);
		assertEquals(1, arrayIndexedCollection.size());
		arrayIndexedCollection.remove(0);
		assertEquals(0, arrayIndexedCollection.size());
	}
	
	
	
	private ArrayIndexedCollection prepareOtherCollection() {
		ArrayIndexedCollection arrayIndexedCollection = new ArrayIndexedCollection();
		assertEquals(16, ArrayIndexedCollection.getCapacity());
		assertEquals(0, arrayIndexedCollection.size());
		assertNotNull(arrayIndexedCollection);

		arrayIndexedCollection.add(new String("A"));
		arrayIndexedCollection.add(new String("B"));
		arrayIndexedCollection.add(new String("C"));

		assertEquals(3, arrayIndexedCollection.size());

		return arrayIndexedCollection;
	}

}

package hr.fer.zemris.java.hw02;

import static org.junit.jupiter.api.Assertions.*;
import static java.lang.Math.*;

import org.junit.jupiter.api.Test;


public class ComplexNumberTest {
	
	@Test
	public void fullComplexNumberConstructorTest() {
		ComplexNumber c = new ComplexNumber(5.3, -2.234);
		assertEquals(5.3, c.getReal());
		assertEquals(-2.234, c.getImaginary());
	}
	
	@Test
	public void pureRealNumberConstructorTest() {
		ComplexNumber c = new ComplexNumber(5.3, 0);
		assertEquals(5.3, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void pureImaginaryNumberConstructorTest() {
		ComplexNumber c = new ComplexNumber(0, -2.234);
		assertEquals(0, c.getReal());
		assertEquals(-2.234, c.getImaginary());
	}
	
	@Test
	public void zeroConstructorTest() {
		ComplexNumber c = new ComplexNumber(0, 0);
		assertEquals(0, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@SuppressWarnings("null")
	@Test
	public void bothPartsNullConstructorTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> new ComplexNumber(val, val));
	}
	
	@SuppressWarnings("null")
	@Test
	public void realPartNullConstructorTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> new ComplexNumber(val, 5));
	}
	
	@SuppressWarnings("null")
	@Test
	public void imaginaryPartNullConstructorTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> new ComplexNumber(5, val));
	}
	
	
	
	@Test
	public void fromRealNonnullNumberTest() {
		ComplexNumber c = ComplexNumber.fromReal(5.4);
		assertEquals(5.4, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@SuppressWarnings("null")
	@Test
	public void fromRealNullTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> ComplexNumber.fromReal(val));
	}
	
	
	
	@Test
	public void fromImaginaryNonnullNumberTest() {
		ComplexNumber c = ComplexNumber.fromImaginary(5.4);
		assertEquals(0, c.getReal());
		assertEquals(5.4, c.getImaginary());
	}
	
	@SuppressWarnings("null")
	@Test
	public void fromImaginaryNullTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> ComplexNumber.fromImaginary(val));
	}
	
	
	// equality assertion done on 2 decimal places
	@Test
	public void fromPositiveMagnitudePositiveAngleTest() {
		ComplexNumber c = ComplexNumber.fromMagnitudeAndAngle(1.2, PI/3);
		assertEquals(0.6, round(c.getReal()*100)/100.0);
		assertEquals(1.04, round(c.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void fromPositiveMagnitudeNegativeAngleTest() {
		ComplexNumber c = ComplexNumber.fromMagnitudeAndAngle(1.2, -PI/3);
		assertEquals(0.6, round(c.getReal()*100)/100.0);
		assertEquals(-1.04, round(c.getImaginary()*100)/100.0);
	}
	
	@Test
	public void fromNegativeMagnitudePositiveAngleTest() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.fromMagnitudeAndAngle(-1, PI));
	}
	
	@Test
	public void fromNegativeMagnitudeNegativeAngleTest() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.fromMagnitudeAndAngle(-1, -PI));
	}
	
	@Test
	public void fromZeroMagnitudeAndAngleTest() {
		ComplexNumber c = ComplexNumber.fromMagnitudeAndAngle(0, PI/3);
		assertEquals(0, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@SuppressWarnings("null")
	@Test
	public void fromNullMagnitudeNullAngleTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> ComplexNumber.fromMagnitudeAndAngle(val, val));
	}
	
	@SuppressWarnings("null")
	@Test
	public void fromNullMagnitudeNonnullAngleTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> ComplexNumber.fromMagnitudeAndAngle(val, 4));
	}
	
	@SuppressWarnings("null")
	@Test
	public void fromNonnullMagnitudeNullAngleTest() {
		Double val = null;
		assertThrows(NullPointerException.class, () -> ComplexNumber.fromMagnitudeAndAngle(4, val));
	}
	
	
	
	@Test
	public void parseFullComplexNumberPositiveRealPositiveImaginaryNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("2.23+4.5i");
		assertEquals(2.23, c.getReal());
		assertEquals(4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberPositiveRealPositiveImaginaryWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  2.23  +  4.5  i  ");
		assertEquals(2.23, c.getReal());
		assertEquals(4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberPositiveRealWithPlusSignPositiveImaginaryNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("+2.23+4.5i");
		assertEquals(2.23, c.getReal());
		assertEquals(4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberPositiveRealWithPlusSignPositiveImaginaryWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  +  2.23  +  4.5  i  ");
		assertEquals(2.23, c.getReal());
		assertEquals(4.5, c.getImaginary());
	}
	
	
	@Test
	public void parseFullComplexNumberPositiveRealNegativeImaginaryNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("2.23-4.5i");
		assertEquals(2.23, c.getReal());
		assertEquals(-4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberPositiveRealNegativeImaginaryWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  2.23  -  4.5  i  ");
		assertEquals(2.23, c.getReal());
		assertEquals(-4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberPositiveRealWithPlusSignNegativeImaginaryNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("+2.23-4.5i");
		assertEquals(2.23, c.getReal());
		assertEquals(-4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberPositiveRealWithPlusSignNegativeImaginaryWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  +  2.23  -  4.5  i  ");
		assertEquals(2.23, c.getReal());
		assertEquals(-4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberNegativeRealPositiveImaginaryNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("-2.23+4.5i");
		assertEquals(-2.23, c.getReal());
		assertEquals(4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberNegativeRealPositiveImaginaryWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  -  2.23  +  4.5  i  ");
		assertEquals(-2.23, c.getReal());
		assertEquals(4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberNegativeRealNegativeImaginaryNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("-2.23-4.5i");
		assertEquals(-2.23, c.getReal());
		assertEquals(-4.5, c.getImaginary());
	}
	
	@Test
	public void parseFullComplexNumberNegativeRealNegativeImaginaryWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  -  2.23  -  4.5  i  ");
		assertEquals(-2.23, c.getReal());
		assertEquals(-4.5, c.getImaginary());
	}
	
	@Test
	public void parsePositiveRealNumberNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("2.23");
		assertEquals(2.23, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parsePositiveRealNumberWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  2.23  ");
		assertEquals(2.23, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parsePositiveRealNumberWithPlusSignNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("+2.23");
		assertEquals(2.23, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parsePositiveRealNumberWithPlusSignWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  +  2.23  ");
		assertEquals(2.23, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parseNegativeRealNumberNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("-2.23");
		assertEquals(-2.23, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parseNegativeRealNumberWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  -  2.23  ");
		assertEquals(-2.23, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parseZeroNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("0");
		assertEquals(0, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parseZeroWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  0  ");
		assertEquals(0, c.getReal());
		assertEquals(0, c.getImaginary());
	}
	
	@Test
	public void parsePositivePureImaginaryNumberNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("2.23i");
		assertEquals(0, c.getReal());
		assertEquals(2.23, c.getImaginary());
	}
	
	@Test
	public void parsePositivePureImaginaryNumberWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  2.23  i  ");
		assertEquals(0, c.getReal());
		assertEquals(2.23, c.getImaginary());
	}
	
	@Test
	public void parseNegativePureImaginaryNumberNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("-2.23i");
		assertEquals(0, c.getReal());
		assertEquals(-2.23, c.getImaginary());
	}
	
	@Test
	public void parseNegativePureImaginaryNumberWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  -  2.23  i  ");
		assertEquals(0, c.getReal());
		assertEquals(-2.23, c.getImaginary());
	}
	
	@Test
	public void parsePositiveImaginaryUnitNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("i");
		assertEquals(0, c.getReal());
		assertEquals(1, c.getImaginary());
	}
	
	@Test
	public void parsePositiveImaginaryUnitWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  i  ");
		assertEquals(0, c.getReal());
		assertEquals(1, c.getImaginary());
	}
	
	@Test
	public void parsePositiveImaginaryUnitWithPlusSignNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("+i");
		assertEquals(0, c.getReal());
		assertEquals(1, c.getImaginary());
	}
	
	@Test
	public void parsePositiveImaginaryUnitWithPlusSignWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  +  i  ");
		assertEquals(0, c.getReal());
		assertEquals(1, c.getImaginary());
	}
	
	@Test
	public void parseNegativeImaginaryUnitNoSpaces() {
		ComplexNumber c = ComplexNumber.parse("-i");
		assertEquals(0, c.getReal());
		assertEquals(-1, c.getImaginary());
	}
	
	@Test
	public void parseNegativeImaginaryUnitWithSpaces() {
		ComplexNumber c = ComplexNumber.parse("  -  i  ");
		assertEquals(0, c.getReal());
		assertEquals(-1, c.getImaginary());
	}
	
	@Test
	public void parseEmptyNumber() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse(""));
	}
	
	@Test
	public void parseNumberInScientificNotation() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("3.44E-2"));
	}
	
	@Test
	public void parseNumberInScientificNotation2() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("3.44e+3"));
	}
	
	@Test
	public void parsePositivePureImaginaryWithUnitBeforeValueNoSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("i12.3"));
	}
	
	@Test
	public void parsePositivePureImaginaryWithUnitBeforeValueWithSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("  i  12.3  "));
	}
	
	@Test
	public void parseNegativePureImaginaryWithUnitBeforeValueNoSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("-i12.3"));
	}
	
	@Test
	public void parseNegativePureImaginaryWithUnitBeforeValueWithSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("  -  i  12.3  "));
	}
	
	@Test
	public void parsePositiveComplexNumberWithUnitBeforeImValueNoSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("3+i12.3"));
	}
	
	@Test
	public void parsePositiveComplexNumberWithUnitBeforeImValueWithSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("  3  +  i  12.3  "));
	}
	
	@Test
	public void parseNegativeComplexNumberWithUnitBeforeImValueNoSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("3-i12.3"));
	}
	
	@Test
	public void parseNegativeComplexNumberWithUnitBeforeImValueWithSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("  3  -  i  12.3  "));
	}
	
	@Test
	public void parseFullComplexNumberWithMoreSignsInARowNoSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("3-+12.3i"));
	}
	
	@Test
	public void parseFullComplexNumberWithMoreSignsInARowWithSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse(" 3 - + 12.3 i "));
	}
	
	@Test
	public void parsePureImaginaryNumberWithMoreSignsInARowNoSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("-+12.3i"));
	}
	
	@Test
	public void parsePureImaginaryNumberWithMoreSignsInARowWithSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse(" - + 12.3 i "));
	}
	
	@Test
	public void parseRealNumberWithMoreSignsInARowNoSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse("-+12.3"));
	}
	
	@Test
	public void parseRealNumberWithMoreSignsInARowWithSpaces() {
		assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse(" - + 12.3 "));
	}
	
	@Test
	public void parseNull() {
		assertThrows(NullPointerException.class, () -> ComplexNumber.parse(null));
	}
	
	
	
	@Test
	public void getRealFromFullComplexNumber() {
		assertEquals(-2.234, new ComplexNumber(-2.234, 5.3).getReal());
	}
	
	@Test
	public void getImaginaryFromFullComplexNumber() {
		assertEquals(5.3, new ComplexNumber(-2.234, 5.3).getImaginary());
	}
	
	@Test
	public void getRealFromPureRealNumber() {
		assertEquals(-2.234, new ComplexNumber(-2.234, 0).getReal());
	}
	
	@Test
	public void getImaginaryFromPureRealNumber() {
		assertEquals(0, new ComplexNumber(-2.234, 0).getImaginary());
	}
	
	@Test
	public void getRealFromPureImaginaryNumber() {
		assertEquals(0, new ComplexNumber(0, 5.3).getReal());
	}
	
	@Test
	public void getImaginaryFromPureImaginaryNumber() {
		assertEquals(5.3, new ComplexNumber(0, 5.3).getImaginary());
	}
	
	@Test
	public void getRealFromZero() {
		assertEquals(0, new ComplexNumber(0, 0).getReal());
	}
	
	@Test
	public void getImaginaryFromZero() {
		assertEquals(0, new ComplexNumber(0, 0).getImaginary());
	}
	
	
	// equality assertion done on 2 decimal places
	@Test
	public void getMagnitudeFullComplexNumber() {
		assertEquals(5.75, round(new ComplexNumber(-2.234, -5.3).getMagnitude()*100)/100.0);
	}
	
	@Test
	public void getMagnitudePureRealNumber() {
		assertEquals(2.234, new ComplexNumber(-2.234, 0).getMagnitude());
	}
	
	@Test
	public void getMagnitudePureImaginaryNumber() {
		assertEquals(5.3, new ComplexNumber(0, -5.3).getMagnitude());
	}
	
	@Test
	public void getMagnitudeZero() {
		assertEquals(0, new ComplexNumber(0, 0).getMagnitude());
	}
	
	
	
	@Test
	public void getAngleFirstQuadrant() {
		assertEquals(PI/4, new ComplexNumber(1, 1).getAngle());
	}
	
	@Test
	public void getAngleSecondQuadrant() {
		assertEquals(3*PI/4, new ComplexNumber(-1, 1).getAngle());
	}
	
	@Test
	public void getAngleThridQuadrant() {
		assertEquals(5*PI/4, new ComplexNumber(-1, -1).getAngle());
	}
	
	@Test
	public void getAngleFourthQuadrant() {
		assertEquals(-PI/4, new ComplexNumber(1, -1).getAngle());
	}
	
	@Test
	public void getAnglePositiveRealNumber() {
		assertEquals(0, new ComplexNumber(3, 0).getAngle());
	}
	
	@Test
	public void getAngleNegativeRealNumber() {
		assertEquals(PI, new ComplexNumber(-3, 0).getAngle());
	}
	
	@Test
	public void getAnglePositivePureImaginaryNumber() {
		assertEquals(PI/2, new ComplexNumber(0, 3).getAngle());
	}
	
	@Test
	public void getAngleNegativePureImaginaryNumber() {
		assertEquals(-PI/2, new ComplexNumber(0, -3).getAngle());
	}
	
	
	// equality assertion done on 2 decimal places
	@Test
	public void addFullToFullComplexNumber() {
		ComplexNumber sum = new ComplexNumber(-2.3, 4.5).add(new ComplexNumber(5.44, -5.6));
		assertEquals(3.14, round(sum.getReal()*100)/100.0);
		assertEquals(-1.1, round(sum.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void addRealToRealNumber() {
		ComplexNumber sum = new ComplexNumber(-2.3, 0).add(new ComplexNumber(5.44, 0));
		assertEquals(3.14, round(sum.getReal()*100)/100.0);
		assertEquals(0, sum.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void addImaginaryToImaginaryNumber() {
		ComplexNumber sum = new ComplexNumber(0, -2.3).add(new ComplexNumber(0, 5.44));
		assertEquals(0, sum.getReal());
		assertEquals(3.14, round(sum.getImaginary()*100)/100.0);
	}
	
	@Test
	public void addImaginaryToRealNumber() {
		ComplexNumber sum = new ComplexNumber(-2.3, 0).add(new ComplexNumber(0, 5.44));
		assertEquals(-2.3, sum.getReal());
		assertEquals(5.44, sum.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void addRealToFullComplexNumber() {
		ComplexNumber sum = new ComplexNumber(-2.3, 4.5).add(new ComplexNumber(5.44, 0));
		assertEquals(3.14, round(sum.getReal()*100)/100.0);
		assertEquals(4.5, sum.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void addImaginaryToFullComplexNumber() {
		ComplexNumber sum = new ComplexNumber(-2.3, 4.5).add(new ComplexNumber(0, -5.6));
		assertEquals(-2.3, sum.getReal());
		assertEquals(-1.1, round(sum.getImaginary()*100)/100.0);
	}
	
	@Test
	public void addZeroToFullComplexNumber() {
		ComplexNumber sum = new ComplexNumber(-2.3, 4.5).add(new ComplexNumber(0, 0));
		assertEquals(-2.3, sum.getReal());
		assertEquals(4.5, sum.getImaginary());
	}
	
	@Test
	public void addZeroToRealNumber() {
		ComplexNumber sum = new ComplexNumber(-2.3, 0).add(new ComplexNumber(0, 0));
		assertEquals(-2.3, sum.getReal());
		assertEquals(0, sum.getImaginary());
	}
	
	@Test
	public void addZeroToImaginaryNumber() {
		ComplexNumber sum = new ComplexNumber(0, 4.5).add(new ComplexNumber(0, 0));
		assertEquals(0, sum.getReal());
		assertEquals(4.5, sum.getImaginary());
	}
	
	@Test
	public void addNullToComplexNumber() {
		assertThrows(NullPointerException.class, () -> new ComplexNumber(0, 4.5).add(null));
	}
	
	
	
	// equality assertion done on 2 decimal places
	@Test
	public void subFullFromFullComplexNumber() {
		ComplexNumber diff = new ComplexNumber(-2.3, 4.5).sub(new ComplexNumber(5.44, -5.6));
		assertEquals(-7.74, round(diff.getReal()*100)/100.0);
		assertEquals(10.1, round(diff.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void subRealFromRealNumber() {
		ComplexNumber diff = new ComplexNumber(-2.3, 0).sub(new ComplexNumber(5.44, 0));
		assertEquals(-7.74, round(diff.getReal()*100)/100.0);
		assertEquals(0, diff.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void subImaginaryFromImaginaryNumber() {
		ComplexNumber diff = new ComplexNumber(0, -2.3).sub(new ComplexNumber(0, 5.44));
		assertEquals(0, diff.getReal());
		assertEquals(-7.74, round(diff.getImaginary()*100)/100.0);
	}
	
	@Test
	public void subImaginaryFromRealNumber() {
		ComplexNumber diff = new ComplexNumber(-2.3, 0).sub(new ComplexNumber(0, 5.44));
		assertEquals(-2.3, diff.getReal());
		assertEquals(-5.44, diff.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void subRealFromFullComplexNumber() {
		ComplexNumber diff = new ComplexNumber(-2.3, 4.5).sub(new ComplexNumber(5.44, 0));
		assertEquals(-7.74, round(diff.getReal()*100)/100.0);
		assertEquals(4.5, diff.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void subImaginaryFromFullComplexNumber() {
		ComplexNumber diff = new ComplexNumber(-2.3, 4.5).sub(new ComplexNumber(0, -5.6));
		assertEquals(-2.3, diff.getReal());
		assertEquals(10.1, round(diff.getImaginary()*100)/100.0);
	}
	
	@Test
	public void subZeroFromFullComplexNumber() {
		ComplexNumber diff = new ComplexNumber(-2.3, 4.5).sub(new ComplexNumber(0, 0));
		assertEquals(-2.3, diff.getReal());
		assertEquals(4.5, diff.getImaginary());
	}
	
	@Test
	public void subZeroFromRealNumber() {
		ComplexNumber diff = new ComplexNumber(-2.3, 0).sub(new ComplexNumber(0, 0));
		assertEquals(-2.3, diff.getReal());
		assertEquals(0, diff.getImaginary());
	}
	
	@Test
	public void subZeroFromImaginaryNumber() {
		ComplexNumber diff = new ComplexNumber(0, 4.5).sub(new ComplexNumber(0, 0));
		assertEquals(0, diff.getReal());
		assertEquals(4.5, diff.getImaginary());
	}
	
	@Test
	public void subNullFromComplexNumber() {
		assertThrows(NullPointerException.class, () -> new ComplexNumber(-2.3, 4.5).sub(null));
	}
	
	
	
	// equality assertion done on 2 decimal places
	@Test
	public void mulFullByFullComplexNumber() {
		ComplexNumber product = new ComplexNumber(-2.3, 4.5).mul(new ComplexNumber(5.44, -5.6));
		assertEquals(12.69, round(product.getReal()*100)/100.0);
		assertEquals(37.36, round(product.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void mulRealByRealNumber() {
		ComplexNumber product = new ComplexNumber(-2.3, 0).mul(new ComplexNumber(5.44, 0));
		assertEquals(-12.51, round(product.getReal()*100)/100.0);
		assertEquals(0, product.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void mulImaginaryByImaginaryNumber() {
		ComplexNumber product = new ComplexNumber(0, -2.3).mul(new ComplexNumber(0, 5.44));
		assertEquals(12.51, round(product.getReal()*100)/100.0);
		assertEquals(0, product.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void mulImaginaryByRealNumber() {
		ComplexNumber product = new ComplexNumber(-2.3, 0).mul(new ComplexNumber(0, 5.44));
		assertEquals(0, round(product.getReal()*100)/100.0);
		assertEquals(-12.51, round(product.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void mulRealByComplexNumber() {
		ComplexNumber product = new ComplexNumber(-2.3, 4.5).mul(new ComplexNumber(5.44, 0));
		assertEquals(-12.51, round(product.getReal()*100)/100.0);
		assertEquals(24.48, round(product.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void mulImaginaryByFullComplexNumber() {
		ComplexNumber product = new ComplexNumber(-2.3, 4.5).mul(new ComplexNumber(0, -5.6));
		assertEquals(25.2, round(product.getReal()*100)/100.0);
		assertEquals(12.88, round(product.getImaginary()*100)/100.0);
	}
	
	@Test
	public void mulZeroByComplexNumber() {
		ComplexNumber product = new ComplexNumber(-2.3, 4.5).mul(new ComplexNumber(0, 0));
		assertEquals(0, round(product.getReal()*100)/100.0);
		assertEquals(0, product.getImaginary());
	}
	
	@Test
	public void mulZeroByNumber() {
		ComplexNumber product = new ComplexNumber(-2.3, 0).mul(new ComplexNumber(0, 0));
		assertEquals(0, round(product.getReal()*100)/100.0);
		assertEquals(0, product.getImaginary());
	}
	
	@Test
	public void mulZeroByImaginaryNumber() {
		ComplexNumber product = new ComplexNumber(0, 4.5).mul(new ComplexNumber(0, 0));
		assertEquals(0, product.getReal());
		assertEquals(0, product.getImaginary());
	}
	
	@Test
	public void mulNullByComplexNumber() {
		assertThrows(NullPointerException.class, () -> new ComplexNumber(-2.3, 4.5).mul(null));
	}
	
	
	
	// equality assertion done on 2 decimal places
	@Test
	public void divFullByFullComplexNumber() {
		ComplexNumber quotient = new ComplexNumber(-2.3, 4.5).div(new ComplexNumber(5.44, -5.6));
		assertEquals(-0.62, round(quotient.getReal()*100)/100.0);
		assertEquals(0.19, round(quotient.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divRealByRealNumber() {
		ComplexNumber quotient = new ComplexNumber(-2.3, 0).div(new ComplexNumber(5.44, 0));
		assertEquals(-0.42, round(quotient.getReal()*100)/100.0);
		assertEquals(0, quotient.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divImaginaryByImaginaryNumber() {
		ComplexNumber quotient = new ComplexNumber(0, -2.3).div(new ComplexNumber(0, 5.44));
		assertEquals(-0.42, round(quotient.getReal()*100)/100.0);
		assertEquals(0, round(quotient.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divImaginaryByRealNumber() {
		ComplexNumber quotient = new ComplexNumber(0, 5.44).div(new ComplexNumber(-2.3, 0));
		assertEquals(0, round(quotient.getReal()*100)/100.0);
		assertEquals(-2.37, round(quotient.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divRealByImaginaryNumber() {
		ComplexNumber quotient = new ComplexNumber(-2.3, 0).div(new ComplexNumber(0, 5.44));
		assertEquals(0, round(quotient.getReal()*100)/100.0);
		assertEquals(0.42, round(quotient.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divFullComplexByRealNumber() {
		ComplexNumber quotient = new ComplexNumber(-2.3, 4.5).div(new ComplexNumber(5.44, 0));
		assertEquals(-0.42, round(quotient.getReal()*100)/100.0);
		assertEquals(0.83, round(quotient.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divRealByFullComplexNumber() {
		ComplexNumber quotient = new ComplexNumber(5.44, 0).div(new ComplexNumber(-2.3, 4.5));
		assertEquals(-0.49, round(quotient.getReal()*100)/100.0);
		assertEquals(-0.96, round(quotient.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divFullComplexByImaginaryNumber() {
		ComplexNumber quotient = new ComplexNumber(-2.3, 4.5).div(new ComplexNumber(0, -5.6));
		assertEquals(-0.8, round(quotient.getReal()*100)/100.0);
		assertEquals(-0.41, round(quotient.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void divImaginaryByFullComplexNumber() {
		ComplexNumber quotient = new ComplexNumber(0, -5.6).div(new ComplexNumber(-2.3, 4.5));
		assertEquals(-0.99, round(quotient.getReal()*100)/100.0);
		assertEquals(0.5, round(quotient.getImaginary()*100)/100.0);
	}
	
	@Test
	public void divZeroByComplexNumber() {
		ComplexNumber quotient = new ComplexNumber(0, 0).div(new ComplexNumber(-2.3, 4.5));
		assertEquals(0, round(quotient.getReal()*100)/100.0);
		assertEquals(0, round(quotient.getImaginary()*100)/100.0);
	}
	
	@Test
	public void divZeroByRealNumber() {
		ComplexNumber quotient = new ComplexNumber(0, 0).div(new ComplexNumber(-2.3, 0));
		assertEquals(0, round(quotient.getReal()*100)/100.0);
		assertEquals(0, round(quotient.getImaginary()*100)/100.0);
	}
	
	@Test
	public void divZeroByImaginaryNumber() {
		ComplexNumber quotient = new ComplexNumber(0, 0).div(new ComplexNumber(0, 4.5));
		assertEquals(0, quotient.getReal());
		assertEquals(0, quotient.getImaginary());
	}
	
	@Test
	public void divComplexNumberByZero() {
		assertThrows(IllegalArgumentException.class, () -> new ComplexNumber(-2.3, 4.5).div(new ComplexNumber(0, 0)));
	}
	
	@Test
	public void divRealNumberByZero() {
		assertThrows(IllegalArgumentException.class, () -> new ComplexNumber(-2.3, 0).div(new ComplexNumber(0, 0)));
	}
	
	@Test
	public void divImaginaryNumberByZero() {
		assertThrows(IllegalArgumentException.class, () ->  new ComplexNumber(0, 4.5).div(new ComplexNumber(0, 0)));
	}
	
	@Test
	public void divComplexNumberByNull() {
		assertThrows(NullPointerException.class, () -> new ComplexNumber(-2.3, 4.5).div(null));
	}
	
	
	
	@Test
	public void fullComplexToPowerZero() {
		ComplexNumber pow = new ComplexNumber(-2.3, 4.5).power(0);
		assertEquals(1, pow.getReal());
		assertEquals(0, pow.getImaginary());
	}
	
	@Test
	public void fullComplexToPowerOne() {
		ComplexNumber pow = new ComplexNumber(-2.3, 4.5).power(1);
		assertEquals(-2.3, pow.getReal());
		assertEquals(4.5, pow.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void fullComplexToPowerTwo() {
		ComplexNumber pow = new ComplexNumber(-2.3, 4.5).power(2);
		assertEquals(-14.96, round(pow.getReal()*100)/100.0);
		assertEquals(-20.7, round(pow.getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void fullComplexToPowerThree() {
		ComplexNumber pow = new ComplexNumber(-2.3, 4.5).power(3);
		assertEquals(127.56, round(pow.getReal()*100)/100.0);
		assertEquals(-19.71, round(pow.getImaginary()*100)/100.0);
	}
	
	@Test
	public void fullComplexToNegativePower() {
		assertThrows(IllegalArgumentException.class, () ->  new ComplexNumber(-2.3, 4.5).power(-1));
	}
	
	@SuppressWarnings("null")
	@Test
	public void fullComplexToNullPower() {
		Integer val = null;
		assertThrows(NullPointerException.class, () ->  new ComplexNumber(-2.3, 4.5).power(val));
	}
	
	@Test
	public void realNumberToPowerZero() {
		ComplexNumber pow = new ComplexNumber(-2.3, 0).power(0);
		assertEquals(1, pow.getReal());
		assertEquals(0, pow.getImaginary());
	}
	
	@Test
	public void realNumberToPowerOne() {
		ComplexNumber pow = new ComplexNumber(-2.3, 0).power(1);
		assertEquals(-2.3, pow.getReal());
		assertEquals(0, pow.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void realNumberToPowerTwo() {
		ComplexNumber pow = new ComplexNumber(-2.3, 0).power(2);
		assertEquals(5.29, round(pow.getReal()*100)/100.0);
		assertEquals(0, pow.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void realNumberToPowerThree() {
		ComplexNumber pow = new ComplexNumber(-2.3, 0).power(3);
		assertEquals(-12.17, round(pow.getReal()*100)/100.0);
		assertEquals(0, pow.getImaginary());
	}
	
	@Test
	public void realNumberToNegativePower() {
		assertThrows(IllegalArgumentException.class, () ->  new ComplexNumber(-2.3, 0).power(-1));
	}
	
	@SuppressWarnings("null")
	@Test
	public void realNumberToNullPower() {
		Integer val = null;
		assertThrows(NullPointerException.class, () ->  new ComplexNumber(-2.3, 0).power(val));
	}
	
	@Test
	public void pureImaginaryNumberToPowerZero() {
		ComplexNumber pow = new ComplexNumber(0, -2.3).power(0);
		assertEquals(1, pow.getReal());
		assertEquals(0, pow.getImaginary());
	}
	
	@Test
	public void pureImaginaryNumberToPowerOne() {
		ComplexNumber pow = new ComplexNumber(0, -2.3).power(1);
		assertEquals(0, pow.getReal());
		assertEquals(-2.3, pow.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void pureImaginaryNumberToPowerTwo() {
		ComplexNumber pow = new ComplexNumber(0, -2.3).power(2);
		assertEquals(-5.29, round(pow.getReal()*100)/100.0);
		assertEquals(0, pow.getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void pureImaginaryNumberToPowerThree() {
		ComplexNumber pow = new ComplexNumber(0, -2.3).power(3);
		assertEquals(0, round(pow.getReal()*100)/100.0);
		assertEquals(12.17, round(pow.getImaginary()*100)/100.0);
	}
	
	@Test
	public void pureImaginaryNumberToNegativePower() {
		assertThrows(IllegalArgumentException.class, () ->  new ComplexNumber(0, -2.3).power(-1));
	}
	
	@SuppressWarnings("null")
	@Test
	public void pureImaginaryNumberToNullPower() {
		Integer val = null;
		assertThrows(NullPointerException.class, () ->  new ComplexNumber(0, -2.3).power(val));
	}

	
	
	@Test
	public void fullComplexFirstRoot() {
		ComplexNumber[] roots = new ComplexNumber(-2.3, 4.5).root(1);
		assertEquals(1, roots.length);
		assertEquals(-2.3, roots[0].getReal());
		assertEquals(4.5, roots[0].getImaginary());
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void fullComplexSecondRoots() {
		ComplexNumber[] roots = new ComplexNumber(-2.3, 4.5).root(2);
		assertEquals(2, roots.length);
		assertEquals(1.17, round(roots[0].getReal()*100)/100.0);
		assertEquals(1.92, round(roots[0].getImaginary()*100)/100.0);
		assertEquals(-1.17, round(roots[1].getReal()*100)/100.0);
		assertEquals(-1.92, round(roots[1].getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void fullComplexThirdRoots() {
		ComplexNumber[] roots = new ComplexNumber(-2.3, 4.5).root(3);
		assertEquals(3, roots.length);
		assertEquals(1.33, round(roots[0].getReal()*100)/100.0);
		assertEquals(1.08, round(roots[0].getImaginary()*100)/100.0);
		assertEquals(-1.6, round(roots[1].getReal()*100)/100.0);
		assertEquals(0.61, round(roots[1].getImaginary()*100)/100.0);
		assertEquals(0.27, round(roots[2].getReal()*100)/100.0);
		assertEquals(-1.69, round(roots[2].getImaginary()*100)/100.0);
	}
	
	// equality assertion done on 2 decimal places
	@Test
	public void fullComplexRootZero() {
		assertThrows(IllegalArgumentException.class, () -> new ComplexNumber(-2.3, 4.5).root(0));
	}
	
	@Test
	public void fullComplexNegativeRoot() {
		assertThrows(IllegalArgumentException.class, () -> new ComplexNumber(-2.3, 4.5).root(-1));
	}
	
	@SuppressWarnings("null")
	@Test
	public void fullComplexNullRoot() {
		Integer val = null;
		assertThrows(NullPointerException.class, () ->  new ComplexNumber(-2.3, 4.5).root(val));
	}
	
	
	
	@Test
	public void fullComplexToString() {
		assertEquals("-2.3+4.5i", new ComplexNumber(-2.3, 4.5).toString());
	}
	
	@Test
	public void negativeRealNumberToString() {
		assertEquals("-2.3", new ComplexNumber(-2.3, 0).toString());
	}
	
	@Test
	public void positiveRealNumberToString() {
		assertEquals("2.3", new ComplexNumber(2.3, 0).toString());
	}
	
	@Test
	public void zeroToString() {
		assertEquals("0", new ComplexNumber(0, 0).toString());
	}
	
	@Test
	public void negativePureImaginaryNumberToString() {
		assertEquals("-4.5i", new ComplexNumber(0, -4.5).toString());
	}
	
	@Test
	public void positivePureImaginaryNumberToString() {
		assertEquals("4.5i", new ComplexNumber(0, 4.5).toString());
	}
	
	@Test
	public void positiveImaginaryUnitToString() {
		assertEquals("i", new ComplexNumber(0, 1).toString());
	}
	
	@Test
	public void negativeImaginaryUnitToString() {
		assertEquals("-i", new ComplexNumber(0, -1).toString());
	}
	
	@Test
	public void fullComplexNumberIncludingPositiveImaginaryUnitToString() {
		assertEquals("-4.5+i", new ComplexNumber(-4.5, 1).toString());
	}
	
	@Test
	public void fullComplexNumberIncludingNegativeImaginaryUnitToString() {
		assertEquals("-4.5-i", new ComplexNumber(-4.5, -1).toString());
	}
	
	
	
}

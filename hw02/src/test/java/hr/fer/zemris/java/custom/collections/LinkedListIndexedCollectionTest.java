package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class LinkedListIndexedCollectionTest {
	
	@Test
	public void defaultConstructorTest() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertNull(linkedListIndexedCollection.getFirst());
		assertNull(linkedListIndexedCollection.getLast());
		assertEquals(0, linkedListIndexedCollection.size());
	}

	@Test
	public void otherCollectionOfSizeThreeConstructorTest() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection(prepareOtherCollection());
		assertEquals(3, linkedListIndexedCollection.size());
	}
	
	@Test
	public void otherCollectionNullConstructorTest() {
		assertThrows(NullPointerException.class, () -> new LinkedListIndexedCollection(null));
	}
	
	

	@Test
	public void collectionOfSizeThreeSize() {
		assertEquals(3, prepareOtherCollection().size());
	}

	@Test
	public void collectionOfSizeZeroSize() {
		assertEquals(0, new LinkedListIndexedCollection().size());
	}

	@Test
	public void collectionThreeNonNullElementsWithDuplicatesAdd() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		linkedListIndexedCollection.add(new String("A"));
		linkedListIndexedCollection.add(new String("B"));
		linkedListIndexedCollection.add(new String("A"));

		assertEquals(3, linkedListIndexedCollection.size());
		assertEquals("A", linkedListIndexedCollection.get(0));
		assertEquals("B", linkedListIndexedCollection.get(1));
		assertEquals("A", linkedListIndexedCollection.get(2));
	}

	@Test
	public void collectionNullElementAdd() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		linkedListIndexedCollection.add(new String("A"));
		linkedListIndexedCollection.add(new String("B"));
		assertThrows(NullPointerException.class, () -> linkedListIndexedCollection.add(null));
	}

	@Test
	public void collectionThreeElementsContainsNonNull() {
		Collection linkedListIndexedCollection = prepareOtherCollection();
		assertTrue(linkedListIndexedCollection.contains("A"));
		assertTrue(linkedListIndexedCollection.contains("B"));
		assertTrue(linkedListIndexedCollection.contains("C"));
	}

	@Test
	public void collectionThreeElementsContainsNull() {
		Collection linkedListIndexedCollection = prepareOtherCollection();
		assertFalse(linkedListIndexedCollection.contains(null));
	}

	@Test
	public void collectionThreeElementsRemoveExisting() {
		Collection linkedListIndexedCollection = prepareOtherCollection();

		assertTrue(linkedListIndexedCollection.contains("A"));
		assertTrue(linkedListIndexedCollection.contains("B"));
		assertTrue(linkedListIndexedCollection.contains("C"));
		assertEquals(3, linkedListIndexedCollection.size());

		assertTrue(linkedListIndexedCollection.remove("C"));

		assertTrue(linkedListIndexedCollection.contains("A"));
		assertTrue(linkedListIndexedCollection.contains("B"));
		assertFalse(linkedListIndexedCollection.contains("C"));
		assertEquals(2, linkedListIndexedCollection.size());
	}

	@Test
	public void collectionThreeElementsRemoveNonexisting() {
		Collection linkedListIndexedCollection = prepareOtherCollection();

		assertTrue(linkedListIndexedCollection.contains("A"));
		assertTrue(linkedListIndexedCollection.contains("B"));
		assertTrue(linkedListIndexedCollection.contains("C"));
		assertFalse(linkedListIndexedCollection.contains("D"));
		assertEquals(3, linkedListIndexedCollection.size());

		assertFalse(linkedListIndexedCollection.remove("D"));

		assertTrue(linkedListIndexedCollection.contains("A"));
		assertTrue(linkedListIndexedCollection.contains("B"));
		assertTrue(linkedListIndexedCollection.contains("C"));
		assertFalse(linkedListIndexedCollection.contains("D"));
		assertEquals(3, linkedListIndexedCollection.size());
	}

	@Test
	public void collectionThreeElementsRemoveNull() {
		Collection linkedListIndexedCollection = prepareOtherCollection();

		assertTrue(linkedListIndexedCollection.contains("A"));
		assertTrue(linkedListIndexedCollection.contains("B"));
		assertTrue(linkedListIndexedCollection.contains("C"));
		assertEquals(3, linkedListIndexedCollection.size());

		assertFalse(linkedListIndexedCollection.remove(null));

		assertTrue(linkedListIndexedCollection.contains("A"));
		assertTrue(linkedListIndexedCollection.contains("B"));
		assertTrue(linkedListIndexedCollection.contains("C"));
		assertEquals(3, linkedListIndexedCollection.size());
	}

	@Test
	public void emptyCollectionToArray() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		Object[] colArray = linkedListIndexedCollection.toArray();

		assertEquals(0, colArray.length);
	}

	@Test
	public void threeElementsCollectionToArray() {
		Object[] colArray = prepareOtherCollection().toArray();

		assertEquals(3, colArray.length);
	}

	@Test
	public void emptyCollectionForEach() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		
		LocalProcessor processor = new LocalProcessor();
		linkedListIndexedCollection.forEach(processor);
		
		assertEquals(0, processor.counter);
	}

	@Test
	public void threeElementsCollectionForEach() {
		Collection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());

		LocalProcessor processor = new LocalProcessor();
		linkedListIndexedCollection.forEach(processor);
		
		assertEquals(3, processor.counter);
	}

	private static class LocalProcessor extends Processor {

		int counter;

		public LocalProcessor() {
		}

		public void process(Object value) {
			counter++;
		}
	}
	
	
	
	@Test
	public void emptyCollectionClear() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());

		linkedListIndexedCollection.clear();
		
		assertEquals(0, linkedListIndexedCollection.size());
	}
	
	@Test
	public void threeElementsCollectionClear() {
		Collection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());

		linkedListIndexedCollection.clear();
		
		assertEquals(0, linkedListIndexedCollection.size());
	}
	
	
	
	@Test
	public void emptyCollectionGetNegativeIndexElement() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection.get(-1));
	}
	
	@Test
	public void emptyCollectionGetPositiveIndexElement() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection.get(1));
	}
	
	@Test
	public void threeElementsCollectionGetNegativeIndexElement() {
		LinkedListIndexedCollection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection .size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection .get(-1));
	}
	
	@Test
	public void threeElementsCollectionGetPositiveOutOfSizeIndex() {
		LinkedListIndexedCollection  linkedListIndexedCollection  = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection .size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection .get(3));
	}
	
	
	@Test
	public void insertElementsIntoEmptyCollection() {
		LinkedListIndexedCollection  linkedListIndexedCollection  = new LinkedListIndexedCollection ();
		assertEquals(0, linkedListIndexedCollection .size());
		
		linkedListIndexedCollection .insert("R", 0);
		linkedListIndexedCollection .insert("Q", 1);
		
		assertEquals(2, linkedListIndexedCollection .size());
	}
	
	@Test
	public void insertElementsIntoEmptyCollectionNegativeIndex() {
		LinkedListIndexedCollection linkedListIndexedCollection  = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection.insert("R", -1));
	}
	
	@Test
	public void insertElementsIntoEmptyCollectionPositiveOutOfSizeIndex() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection.insert("R", 1));
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollection() {
		LinkedListIndexedCollection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());
		
		linkedListIndexedCollection.insert("R", 3);
		linkedListIndexedCollection.insert("Q", 4);

		assertEquals(5, linkedListIndexedCollection.size());
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollectionNegativeIndex() {
		LinkedListIndexedCollection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection.insert("R", -1));
	}
	
	@Test
	public void insertElementsIntoThreeElementsCollectionPositiveOutOfSizeIndex() {
		LinkedListIndexedCollection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection.insert("R", 4));
	}
	
	
	
	@Test
	public void indexOfElementsInEmptyCollection() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		
		assertEquals(-1, linkedListIndexedCollection.indexOf(4));
		assertEquals(-1, linkedListIndexedCollection.indexOf(null));
	}
	
	@Test
	public void indexOfExistingElementsInThreeElementsCollection() {
		LinkedListIndexedCollection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());
		
		assertEquals(0, linkedListIndexedCollection.indexOf("A"));
		assertEquals(1, linkedListIndexedCollection.indexOf("B"));
		assertEquals(2, linkedListIndexedCollection.indexOf("C"));
	}
	
	@Test
	public void indexOfNonexistingElementsInThreeElementsCollection() {
		LinkedListIndexedCollection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());
		
		assertEquals(-1, linkedListIndexedCollection.indexOf(4));
		assertEquals(-1, linkedListIndexedCollection.indexOf(null));
	}
	
	
	
	@Test
	public void removeAtIndexFromEmptyCollection() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		
		assertThrows(IndexOutOfBoundsException.class, () -> linkedListIndexedCollection.remove(0));
	}
	
	@Test
	public void removeAtAllowedIndexFromThreeElementCollection() {
		LinkedListIndexedCollection linkedListIndexedCollection = prepareOtherCollection();
		assertEquals(3, linkedListIndexedCollection.size());
		
		linkedListIndexedCollection.remove(2);
		assertEquals(2, linkedListIndexedCollection.size());
		linkedListIndexedCollection.remove(1);
		assertEquals(1, linkedListIndexedCollection.size());
		linkedListIndexedCollection.remove(0);
		assertEquals(0, linkedListIndexedCollection.size());
	}
	
	
	
	private LinkedListIndexedCollection prepareOtherCollection() {
		LinkedListIndexedCollection linkedListIndexedCollection = new LinkedListIndexedCollection();
		assertEquals(0, linkedListIndexedCollection.size());
		assertNotNull(linkedListIndexedCollection);

		linkedListIndexedCollection.add(new String("A"));
		linkedListIndexedCollection.add(new String("B"));
		linkedListIndexedCollection.add(new String("C"));

		assertEquals(3, linkedListIndexedCollection.size());

		return linkedListIndexedCollection;
	}


}
